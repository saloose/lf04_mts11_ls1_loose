import java.util.Scanner;

public class ForAufgabe8 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie bitte die Seitenlaenge des Quadrats an:");
		int laenge = scan.nextInt();
		
		for(int i = 1; i <= laenge; i++) {
			for(int j = 1; j <= laenge; j++) {
				if(i == 1 || i == laenge) {
					System.out.print("*");
				}else if(j == 1 || j == laenge) {
					System.out.print("*");
				}else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		scan.close();
	}

}
