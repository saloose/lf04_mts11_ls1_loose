import java.util.Scanner;

public class WhileAufgabe2 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie einer Zahl zwischen 0 und 20 ein:");
		int n = scan.nextInt();
		long ergebnis = 1;
		while(n > 0) {
			ergebnis *= n;
			n--;
		}
		System.out.println("Die Fakultaet der Zahl ist " + ergebnis );
	}

}
