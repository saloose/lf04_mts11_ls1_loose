import java.util.Scanner;

public class WhileAufgabe1 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Bis zu welcher Zahl soll gezaehlt werden?");
		int n = scan.nextInt();
		int i = 1;
		while (i <= n) {
			System.out.println(i);
			i++;
		}
		System.out.println("Von welcher Zahl aus soll heruntergezaehlt werden:");
		n = scan.nextInt();
		while (n > 0) {
			System.out.println(n);
			n--;
		}
	}

}
