
public class ForAufgabe4 {

	public static void main(String[] args) {
		//a
		System.out.println("Aufgabe 4a:");
		for(int i = 99; i >= 9; i-=3) {
			System.out.println(i);
		}
		System.out.println();
		//b
		System.out.println("Aufgabe 4b:");
		for(int i = 1; i <= 20; i++) {
			System.out.println(i*i);
		}
		System.out.println();
		//c
		System.out.println("Aufgabe 4c:");
		for(int i = 2; i <= 102; i+=4) {
			System.out.println(i);
		}
		System.out.println();
		//d
		System.out.println("Aufgabe 4d:");
		for(int i = 2; i<= 32; i+=2) {
			System.out.println(i*i);
		}
		System.out.println();
		//e
		System.out.println("Aufgabe 4e");
		for(int i = 2; i <= 32768; i*=2) {
			System.out.println(i);
		}
	}

}
