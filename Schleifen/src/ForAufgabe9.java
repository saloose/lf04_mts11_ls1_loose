import java.util.Scanner;

public class ForAufgabe9 {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int h;
		int b;
		System.out.println("Geben Sie bitte die Hoehe der Treppe ein:");
		h = scan.nextInt();
		System.out.println("Geben Sie bitte die Stufenbreite ein:");
		b = scan.nextInt();
		for(int i = 1; i <= h; i++) {
			for(int j = h*b; j > 0; j--) {
				if(j > i*b) {
					System.out.print(" ");
				}else {
					System.out.print("*");
				}	
			}
			System.out.println();
		}
		scan.close();
	}
}
