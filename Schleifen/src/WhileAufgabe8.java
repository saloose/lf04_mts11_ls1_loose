import java.util.Scanner;

public class WhileAufgabe8 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl zwischen 2 und 9 ein:");
		int x = scan.nextInt();
		int i = 0;
		int ziffer1 = 0;
		int ziffer2 = 0;
		while(i < 100) {
			if(i % 10 == 0) {
				System.out.println();
			}
			ziffer1 = i / 10;
			ziffer2 = i - (ziffer1 * 10);
			if(ziffer1 == x || ziffer2 == x || i %  x == 0 || (ziffer1 + ziffer2) == x ) {
				System.out.print(" * ");
			}else {
				System.out.printf("%3d",i);
			}
			i++;
		}
		scan.close();
	}

}
