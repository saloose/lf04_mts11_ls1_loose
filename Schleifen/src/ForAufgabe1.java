import java.util.Scanner;

public class ForAufgabe1 {

	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int n;
		System.out.println("Bis zu welcher Zahl soll gezaehlt werden?");
		n = scan.nextInt();
		for(int i = 1; i <= n; i++) {
			System.out.println(i);
		}
		System.out.println("Von welcher Zahl aus soll runtergezaehlt werden?");
		n = scan.nextInt();
		for(int i = n; i > 0; i--) {
			System.out.println(i);
		}
		scan.close();
	}
}
