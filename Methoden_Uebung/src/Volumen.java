
public class Volumen {
	
	public static double wuerfel(double a) {
		return a*a*a;
	}
	
	public static double quader(double a, double b, double c) {
		return a * b * c;
	}
	
	public static double pyramide(double a, double h) {
		return a * a * h / 3;
	}
	
	public static double kugel(double r) {
		return 4/3 * r * r * r * Math.PI; 
	}
	
	public static void main(String[] args) {
		double a = 2.5;
		double b  = 3.5;
		double c = 1.5;
		
		System.out.println("Volumen des Wuerfels mit Kantenlaenge " + a + ": " + wuerfel(a));
		System.out.println("Volumen des Quaders mit Kantenlaengen " + a + " " + b + " " + c + ": " + quader(a, b, c));
		System.out.println("Volumen der Pyramide mit Kantenlaenge " + a + " und Hoehe " + b + ": " + pyramide(a, b));
		System.out.println("Volumen der Kugel mit Radius " + a + ": " + kugel(a));
		
	}
}
