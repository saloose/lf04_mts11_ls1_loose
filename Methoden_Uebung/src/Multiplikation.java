public class Multiplikation {
	
	
	public static void main(String[] args) {
		double wert1 = 2.36;
		double wert2 = 7.87;
		double ergebnis = multiplizieren(wert1, wert2);
		System.out.println(ergebnis);
		
	}
	
	public static double multiplizieren(double a, double b) {
		return a * b;
	}
}
