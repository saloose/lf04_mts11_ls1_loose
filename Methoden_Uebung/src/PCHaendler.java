import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString(scan, "was moechten Sie bestellen?");

		int anzahl = liesInt(scan, "Geben Sie die Anzahl ein:");

		double preis = liesDouble(scan, "Geben Sie den Nettopreis ein:");

		double mwst = liesDouble(scan, "Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		

	}

	public static String liesString(Scanner scan, String text) {
		String ret;
		System.out.println(text);
		ret = scan.next();
		return ret;
	}
	
	public static int liesInt(Scanner scan, String text) {
		int ret;
		System.out.println(text);
		ret = scan.nextInt();
		return ret;
	}
	
	public static double liesDouble(Scanner scan, String text) {
		double ret;
		System.out.println(text);
		ret = scan.nextDouble();
		return ret;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}

	public static void rechnungausgeben( String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}