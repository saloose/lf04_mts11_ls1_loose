import java.util.Scanner;

public class Aufgabe2 {
	
	
	public static void main(String[] args) {
		double nettowert;
		double bruttowert = 0;
		char reduziert;
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie bitte den Nettowert ein:");
		nettowert = scan.nextDouble();
		System.out.println("Soll der reduzierte Steuersatz verwendet werden? j/n?");
		reduziert = scan.next().charAt(0);
		switch(reduziert) {
			case 'j':
				bruttowert = 1.07*nettowert;
				break;
			case 'n':
				bruttowert = 1.19*nettowert;
				break;
			default:
				System.out.println("Bitte nur mit j oder n antworten");
				System.exit(0);
		}
		System.out.println("Der Bruttobetrag ist: " + bruttowert);
		scan.close();
	}
}
