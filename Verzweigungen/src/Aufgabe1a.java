import java.util.Scanner;

public class Aufgabe1a {
	
	
	public static void main(String[] args) {
		double a, b;
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie bitte die erste Zahl ein:");
		a = scan.nextDouble();
		System.out.println("Geben Sie bitte die zweite Zahl ein:");
		b = scan.nextDouble();
		gleich(a, b);
		bgroessera(a, b);
		agroesserb(a, b);
		scan.close();
	}
	
	public static void gleich(double a, double b) {
		if(a == b) {
			System.out.println(a + " und " + b + " sind gleich gross!");
		}
	}
	
	public static void bgroessera(double a, double b) {
		if(b > a) {
			System.out.println(b + " ist groesser als " + a);
		}
	}
	
	public static void agroesserb(double a, double b) {
		if(a > b) {
			System.out.println(a + " ist groesser als " + b);
		} else {
			System.out.println(a + " ist nicht groesser als " + b);
		}
	}
}
