import java.util.Scanner;

public class Aufgabe1b {
	
	public static void main(String[] args) {
		double a, b, c;
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie bitte die erste Zahl ein:");
		a = scan.nextDouble();
		System.out.println("Geben Sie bitte die zweite Zahl ein:");
		b = scan.nextDouble();
		System.out.println("Geben Sie bitte die dritte Zahl ein:");
		c = scan.nextDouble();
		agroesserbundc(a, b, c);
		cgroesseraorderb(a, b, c);
		groesste(a, b, c);
		scan.close();
	}
	
	public static void agroesserbundc(double a, double b, double c) {
		if(a > b && a > c){
			System.out.println(a + " ist groesser als " + b + " und " + c);
		}
	}
	
	public static void cgroesseraorderb(double a, double b, double c) {
		if(c > a || c > b) {
			System.out.println(c + " ist groesser als " + b + " oder " + a);
		}
	}
	
	public static void groesste(double a, double b, double c) {
		if(a > b && a > c) {
			System.out.println(a + " ist die groesste der drei Zahlen");
		} else if (b > a && b > c){
			System.out.println(b + " ist die groesste der drei Zahlen");
		} else if (c > a && c > b){
			System.out.println(c + " ist die groesste der drei Zahlen");
		}
	}
}
