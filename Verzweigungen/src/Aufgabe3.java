import java.util.Scanner;

public class Aufgabe3 {
	
	public static void main(String[] args) {
		int anzahl;
		double preis;
		double rechnungsbetrag;
		Scanner scan = new Scanner(System.in);
		System.out.println("Wie viele Maeuse wurden bestellt?");
		anzahl = scan.nextInt();
		System.out.println("Was ist der Einzelpreis einer Maus?");
		preis = scan.nextDouble();
		
		rechnungsbetrag = anzahl * preis * 1.19;
		if (anzahl < 10) {
			rechnungsbetrag += 10.0;
		}
		System.out.printf("Der Rechnungsbetrag ist: %.2f\n", rechnungsbetrag);
		scan.close();
	}
}
