﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double rueckgabebetrag;
       
       while(true) {
	       // Bestellung erfassen
	       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
	       
	       // Geldeinwurf
	       // -----------
	       rueckgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
	
	       // Fahrscheinausgabe
	       // -----------------
	       fahrkartenAusgeben();
	
	       // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	       rueckgeldAusgeben(rueckgabebetrag);
       }
    }
    
    public static double fahrkartenbestellungErfassen(Scanner scan) {
    	double zuZahlenderBetrag = 0.0;
    	int anzahlTickets;
    	int tickettyp = -1;
		String ticketname[] = {	"Einzelfahrschein Berlin AB",
								"Einzelfahrschein Berlin BC",
								"Einzelfahrschein Berlin ABC",
								"Kurzstrecke",
								"Tageskarte Berlin AB",
								"Tageskarte Berlin BC",
								"Tageskarte Berlin ABC",
								"Kleingruppen-Tageskarte Berlin AB",
								"Kleingruppen-Tageskarte Berlin BC",
								"Kleingruppen-Tageskarte Berlin ABC"				
								};
		double ticketpreis[] = { 2.9,
								 3.3,
								 3.6,
								 1.9,
								 8.6,
								 9.0,
								 9.6,
								 23.5,
								 24.3,
								 24.9				
								};

		while(tickettyp != 0){
			tickettyp = -1;
			anzahlTickets = 0;
			System.out.println("Wählen Sie ihre Wunschfahrkarte aus:");
			for(int i = 0; i < ticketname.length; i++) {
				System.out.println("[" + (i+1) + "] " + ticketname[i]);
			}
			System.out.println("[0] Bezahlen");
			System.out.println();
			//Auswahl der Ticketart
			while(tickettyp == -1) {
				System.out.print("Ihre Wahl: ");
				tickettyp = scan.nextInt();
				if(tickettyp != 0) {
					if(tickettyp < 0 && tickettyp > ticketname.length) {
						System.out.println("Ungueltige Eingabe");
						tickettyp = -1;
					}
				}
			}
			//Anzahl der Tickets
			if(tickettyp != 0){
				System.out.print("Anzahl der Tickets: ");
				while(anzahlTickets == 0){
					anzahlTickets = scan.nextInt();
					if(anzahlTickets <= 0 || anzahlTickets > 10){
						System.out.println(">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
						anzahlTickets = 0;
					}
				}
			//Zwischensumme berechnen und ausgeben
				zuZahlenderBetrag += (double) anzahlTickets * ticketpreis[tickettyp -1];
				System.out.printf("Zwischensumme: %.2f €\n\n",zuZahlenderBetrag);
			}
		}	
    	return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(Scanner scan, double zuZahlen) {
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;

        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = scan.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	return eingezahlterGesamtbetrag - zuZahlen;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rueckgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n");
    }
}

/*
2.5.5 Datentyp int: Ganzzahl, da es keine Ticketbruchteile geben soll (wenn Speichermenge ein Problem ist könnte auch byte oder short verwendet werden)
2.5.6 Bei Anzahl * Preis wird zuerst die anzahl implizit zu double gecastet (da der andere teil der multiplikation ein double ist) und dann mit dem Preis multipliziert. Danach wird das ergebnis der variable zugewiesen

*/