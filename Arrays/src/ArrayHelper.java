
public class ArrayHelper {

	public static String convertArrayToString(int [] zahlen) {
		String result = "";
		for(int i = 0; i < zahlen.length; i++) {
			result += zahlen[i];
			if(i != zahlen.length - 1) {
				result += ", ";
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		int test[] = new int[] {2, 3, 4};
		System.out.println(convertArrayToString(test));
	}

}
