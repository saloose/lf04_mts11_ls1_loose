
public class Aufgabe4 {
	
	public static double[][] temperaturtabelle(int anzahl) {
		double tabelle[][] = new double[anzahl][2];
		for(int i = 0; i < anzahl; i++) {
			tabelle[i][0] = 10.0 * i;
			tabelle[i][1] = (5.0/9.0) * (tabelle[i][0] - 32.0);
		}
		return tabelle;
	}

	public static void main(String[] args) {
		double temperaturen[][] = temperaturtabelle(5);
		for(int i = 0; i < temperaturen.length; i++) {
			System.out.printf("%5.2f | %5.2f\n", temperaturen[i][0], temperaturen[i][1]);
		}
	}

}
