
public class Aufgabe6 {
	
	public static boolean symmetrisch(int [][] matrix) {
		for(int i = 0; i < matrix.length; i++) {
			for(int j = i+1; j < matrix.length; j++) {
				if(matrix[i][j] != matrix[j][i]) {
					return false;
				}
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		int testmatrix1[][] = new int[][] {{1,2,3},{2,3,1},{3,1,2}};
		int testmatrix2[][] = new int[][] {{1,2,3},{1,2,3},{1,2,3}};
		int testmatrix3[][] = new int[][] {{1}};
		System.out.println(symmetrisch(testmatrix1));
		System.out.println(symmetrisch(testmatrix2));
		System.out.println(symmetrisch(testmatrix3));

	}
}
