import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		System.out.println("Hallo! Wie ist dein Name?");
		String name = eingabe.next();
		System.out.println("Wie alt bist du?");
		int alter = eingabe.nextInt();
		System.out.println("Dein name ist " + name + " und du bist " + alter + " Jahre alt.");
		eingabe.close();
	}

}
